# Oracle Autonomous Database with Node
La conexión programática hacia una base de datos en la nube a través de un lenguaje de programación difiere un poco
a la conexión a través de una herramienta SQL como lo es sqlcl, sqlplus, Data Modeler, Datagrip, etc.
<br>
Este ejemplo incluye instrucciones y la implementación de una pequeña aplicación escrita en Node para conectarse 
a una base de datos en la nube de Oracle.

## Obten tu Connection Wallet.
Una connection wallet, o simplemente wallet es un archivo zip que contiene varios archivos de autenticación que
utilizan las librerías de conexión para autenticarse con la base de datos en la nube sobre el protocolo tcps y 
también posee un archivo clave llamado tnsnames.ora en el que se resuelven las direcciones de host. Sin esta wallet
no podrás conectarte a tu base de datos por ningún método. La wallet puedes descargarla cuando creas tu base de
datos en el portal web de Oracle, una vez creada también puedes descargar tu wallet haciendo click en el botón
``DB Connection``. No debes compartir tu wallet con nadie, a menos que necesites que esa persona se conecte a
tu base de datos. Si sientes que tu wallet se ha visto comprometida, puedes crear una nueva e inhabilitar la 
anterior. Una vez descargues tu wallet, debes descomprimirla en algún directorio seguro de tu elección, por ejemplo:
```zsh
unzip -d /home/$USER/Oracle ~/Downloads/wallet.zip
```

## Descarga las librerías de conexión de Oracle.
Sin importar que lenguaje de programación utilices, las librerías de conexión de Oracle son obligatorias para
poder conectar, y son las mismas independientemente del lenguaje de programación que estés utilizando.
Si ya has instalado una versión de Oracle física en tu computadora, no debes descargar nada extra, ya que estas
instalaciones incluyen estas librerías por default. Simplemente debes configurar tu entorno para poder utilizarlas
si es que aún no lo has hecho. Si no cuentas con una instalación de Oracle física, deberás descargar las librerías
del sitio oficial de Oracle, llamadas ``Oracle Instant Client``. El descargarlas te baja un archivo .zip con todas
las librerías que necesitas. Una vez descagado, debes descomprimir el archivo en algún directorio seguro distinto
al directorio donde descomprimiste tu wallet. Por ejemplo:

```zsh
unzip -d /home/$USER/InstantClient ~/Downloads/instant-client.zip
```

Donde la ruta del primer parámetro puedes sustituirla por cualquier ruta que desees. Luego solo debes editar
el archivo env.sh para utilizar las nuevas librerías.

## Edita el archivo sqlnet.ora en tu Wallet.
Una vez descomprimas tu wallet, habrá un archivo llamado sqlnet.ora entre los varios archivos de conexión que encontrarás. Debes abrir este archivo y editarlo, de modo que sustituyas el valor de la variable DIRECTORY y coloques la ruta
absoluta del directorio donde se encuentran todos los archivos de tu wallet. Debes localizar una ruta que por default
tiene un signo de interrogación y la ruta network/admin. Debes borrar el signo de interrogación y cambiar la ruta de
muestra por la ruta correcta.

## Configura tu entorno
Las librerías oficiales de Oracle dependen bastante de tu entorno, debes estar seguro de tenerlo bien configurado
antes de intentar correr tu aplición, puedes guiarte del archivo de configuración de entorno adjunto en este
repositorio. Los valores de las rutas debes ajustarlos para que correspondan a tu entorno. Todas las variables
del archivo env son obligatorias, así que asegurate de tomarlo como muestra y editarlo apropiadamente. Cuando
esté listo, puedes importarlo con el comando:

```
. ./env.sh
```
Recuerda, no debes subir tu verdadero archivo de entorno al repositorio ya que este contiene tus credenciales!
En este repositorio se encuentra ya que es con motivos didácticos, 
pero los archivos de entorno nunca se incluyen en ningún proyecto.

<br>
Para conocer el nombre del servicio de tu base de datos, puedes inspeccionar el archivo tnsnames.ora el cual
contiene el nombre de los 3 servicios de base de datos que puedes utilizar, por lo general tienen el formato:

```zsh
DBNAME_medium
```

Donde medium puede cambiar por las palabras high o low.

Una configurado tu entorno, puedes correr el comando make && ./oci 
El gitignore ya está configurado para no incluir tu wallet en el 
repositorio, si en caso has decidido extraerla aquí.
<br>
La mayor parte de los problemas que pueden surgir durante la ejecución de esta aplicación es que aparezcan mensajes
de error de que no se encuentran las shared libraries de Oracle, si eso pasa, asegurate de tener bien configurado
tu entorno y prueba de nuevo. Puedes seguir el tutorial oficial para la instalación de Oracle Instant Client
libraries o seguir mi tutorial para la instalación de una base de datos Oracle física que incluye todas estas 
librerías por defecto.
