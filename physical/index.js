const oracledb = require('oracledb');

async function run() {

  let connection;

  try {

    connection = await oracledb.getConnection({ 
        user: process.env.DB_USER, 
        password: process.env.DB_PASS, 
        connectString: `(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = ${process.env.DB_HOST})(PORT = ${process.env.DB_PORT}))(CONNECT_DATA =(SERVICE_NAME = ${process.env.DB_NAME})))`
    });

    const result = await connection.execute(`SELECT * FROM mytable`);

    console.dir(result.rows, { depth: null });

  } catch (err) {
    console.error(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}

run();
