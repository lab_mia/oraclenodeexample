export ORACLE_BASE=/u01/app/oracle # Solo aplica si tienes una instalación física de Oracle
export ORACLE_HOME=$ORACLE_BASE/product/19.3.0/dbhome_1 #Igual que arriba, ignora si utilizas Instant Client
export CLASSPATH=$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib #Solo aplica para instalaciones físicas

export TNS_ADMIN=/tmp/node-example/oci
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib 
export PATH=$PATH:$LD_LIBRARY_PATH

# Connection credentials (It's better if you set them on a separate environment, do not commit them!)

export DB_USER=RENATO
export DB_PASS=oracle
export DB_HOST=129.159.92.217
export DB_PORT=1521
export DB_NAME=MIA
